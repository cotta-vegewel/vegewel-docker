FROM ruby:2.7.3
RUN apt-get update -qq \
    && apt-get install -y nodejs

ADD ./vegewel /app
WORKDIR /app
RUN gem install bundler -v 2.3.15
EXPOSE 3000

ADD ./admin-vegewel /app
WORKDIR /app
RUN gem install bundler -v 2.3.15
EXPOSE 3001

ADD ./apps-goodgoodmart /app
WORKDIR /app
RUN gem install bundler -v 2.2.20
RUN gem install -N libv8 -v '3.16.14.13' -- --with-system-v8 \
 && bundle config --local build.libv8 --with-system-v8
EXPOSE 3002
CMD ["bash"]
