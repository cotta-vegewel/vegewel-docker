# 使い方

1. DB フォルダに DB のダンプの sql ファイルを入れる。
1. vegewel フォルダ(AdminVegewelであればadmin-vegewelフォルダ、apps-goodgoodmartなら同名のフォルダを作成）にレポジトリを clone。
1. environment.env.sample を environment.env にコピー、必要な環境変数を設定
1. docker-compose build で中身をビルドする
1. docker-compose.yml の開発で使うアプリの項のコメントを外す。Vegewel（サービス）ならweb-vegewel、AdminVegewelなら web-adminvegewel、GGMならweb-ggmのコメントアウトを蓮して、以外の項をコメントアウトする
1. docker-compose up で起動。(一回目は DB に接続できないエラーでてきて、もう一度起動したら繋がります。)

とりあえず自分の（ザンドラさんの）パソコンで動くようにしただけなので、もしかしたら動かないのかもしれません。4.のエラーをはじめいろいろ改善できそうなのでぜひご対応ください。

# シェルでWEBサーバーログインしたいとき（web-vegewelは適宜web-adminvegewel, web-ggmに読み替えすること）
```
docker-compose exec web-vegewel hash
```
または
```
docker exec -it vegewel-docker_web_1 bash
```

# シェルでDBサーバーログインしたいとき
```
docker-compose exec db bash
```
または
```
docker exec -it vegewel-docker_db_1 bash
```

## MySQLの中にログイン
```
mysql -u root -ppassword
```

### DBダンプを手動でインポートする方法
```
$ docker cp ダンプファイル.sql db:/tmp/dump.sql
$ docker-compose exec db bash
root@eb613f8e8b6d:/# mysql -u root -ppassword vegan_prot < /tmp/ダンプファイル名
```

### DBダンプをインポート中にエラーになった場合
`packet for query is too large. Try adjusting the 'max_allowed_packet' variable on the server` でパケットサイズが小さいと言われた場合以下のように対処。
サイズを確認して増やす。

```
$ docker exec -it vegewel-docker_db_1 bash
root@eb613f8e8b6d:/# mysql -u root -ppassword
mysql: [Warning] Using a password on the command line interface can be insecure.
Welcome to the MySQL monitor.  Commands end with ; or \g.
Your MySQL connection id is 2
Server version: 5.7.32 MySQL Community Server (GPL)

Copyright (c) 2000, 2020, Oracle and/or its affiliates. All rights reserved.

Oracle is a registered trademark of Oracle Corporation and/or its
affiliates. Other names may be trademarks of their respective
owners.

Type 'help;' or '\h' for help. Type '\c' to clear the current input statement.

mysql> SHOW VARIABLES LIKE 'max_allowed_packet';
+--------------------+---------+
| Variable_name      | Value   |
+--------------------+---------+
| max_allowed_packet | 4194304 |
+--------------------+---------+
1 row in set (0.03 sec)

mysql> SET GLOBAL max_allowed_packet=41943040;
Query OK, 0 rows affected (0.01 sec)
```

上記に加えて、dbディレクトリにmy.cnfファイルを設置して以下の内容を記載しても同様の効果がある。
```
[mysqld]
max_allowed_packet=128MB
```

### アプリの動作が怪しいとき
```
docker-compose build
```
で治ることがあるので試してみる

### コンテナーが動いているかを確認する
以下のコマンドでStateがdbもwebもupになっていることを確認する。
もしexitとなっていたら何かがおかしい。
```
$ docker-compose ps
        Name                      Command              State              Ports
-------------------------------------------------------------------------------------------
vegewel-docker_db_1    docker-entrypoint.sh mysqld     Up      0.0.0.0:3306->3306/tcp,
                                                               33060/tcp
vegewel-docker_web_1   bash -c rm -f tmp/pids/ser      Up      0.0.0.0:3000->3000/tcp
```
 もしくは
 ```
 $ docker ps
CONTAINER ID   IMAGE                COMMAND                  CREATED        STATUS              PORTS                               NAMES
fc0e9a588a70   vegewel-docker_web   "bash -c 'rm -f tmp/…"   9 hours ago    Up About a minute   0.0.0.0:3000->3000/tcp              vegewel-docker_web_1
eb613f8e8b6d   mysql:5.7.32         "docker-entrypoint.s…"   10 hours ago   Up About a minute   0.0.0.0:3306->3306/tcp, 33060/tcp   vegewel-docker_db_1
```

